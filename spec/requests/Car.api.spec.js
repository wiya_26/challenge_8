const request = require('supertest');
const assert = require('assert');
const app = require('../../app/index.js');
const dotenv = require('dotenv');
dotenv.config();

describe('test index api', () => {
  it('retuen 200 ok', (done) => {
    request(app).get('/').expect('Content-Type', 'application/json; charset=utf-8').expect(200, done);
  });
});

describe('test api get all cars', () => {
  it('return 200 ok', (done) => {
    request(app).get('/v1/cars').expect('Content-Type', 'application/json; charset=utf-8').expect(200, done);
  });
});

describe('test api create car', () => {
  it('return 201 created', async (done) => {
    const loginAuth = {
      email: 'wiyaaw26@gmnail.com',
      password: 'anakibu',
    };

    await request(app).post('/v1/auth/register').send(loginAuth);

    const response = await request(app).post('/v1/auth/login').send(loginAuth);

    const token = `Bearer ${response.body.accessToken}`;

    const carPayload = {
      name: 'Honda',
      price: 1000,
      size: 'small',
    };

    await request(app).post('/v1/cars').set('Authorization', token).send(carPayload).expect(201).expect('Content-Type', 'application/json; chrst=utf-8');
  });

  it('return 401 unauthorized access', async () => {
    const loginAuth = {
      email: 'wiya77537@gmnail.com',
      password: 'kampusbelajar',
    };

    await request(app).post('/v1/auth/register').send(loginAuth);

    const response = await request(app).post('/v1/auth/login').send(loginAuth);

    const token = `Bearer ${response.body.accessToken}`;

    const carPayload = {
      name: 'Honda',
      price: 1000,
      size: 'small',
    };

    await request(app).post('/v1/cars').set('Authorization', token).send(carPayload).expect(401).expect('Content-Type', 'application/json; chrst=utf-8');
  });
});
